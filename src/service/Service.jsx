import axios from "axios";


const API = axios.create({
    baseURL: "http://110.74.194.124:3034/api"
})


export const fetchAllAuthor = async ()=>{
    const result = await API.get("/author")
    return result.data.data
}

export const changeId = (idx) => {

}

//Add Author
export const fetchAddAuthor = async (author) =>{
    try {
        const result = await API.post("/author", author)
        return result
    } catch (error) {
        console.log("AddAuthorError: ", error);
    }
}

//update author by ID
export const upadateAuthorById = async(idx, author)=>{
    try {
      const result = await API.put(`/author/${idx}`, author)
      console.log("updateAuthorById:", result.data.data);
      return result
    } catch (error) {
      console.log("updateAuthorById Error:", error);
    }
  }


//Delete Author
export const deleteAuthorById = async (idx) =>{
    const result = await API.delete(`/author/${idx}`)
    return result
}

//upload image to api
export const uploadImage = async (image) => {
    try {
     const fd = new FormData();
     fd.append("image", image)
     const result = await API.post("/images", fd)
     return result.data.url
    } catch (error) {
        console.log("uploadImage Error:", error);
    }
 }