import React, { useEffect, useState } from 'react'
import { Form, Button, Row, Table } from 'react-bootstrap'
import { uploadImage, fetchAddAuthor, upadateAuthorById, deleteAuthorById, fetchAllAuthor } from '../service/Service'

export default function MyAuthor() {


    const [authorName, setauthorName] = useState("")
    const [email, setemail] = useState("")
    let [MyImage, setMyImage] = useState("")
    let [browsedImage, setBrowsedImage] = useState("")
    const [author, setauthor] = useState([])
    const [id, setId] = useState("")
    const [isSave, setIsSave] = useState(true)

    const pattern = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z]+$/
    // let tempId = document.getElementById('hidden-id')
    // setId(tempId)

    const placeholder = "images/placeholder.png"

    useEffect(async () => {
        const result = await fetchAllAuthor()
        setauthor(result)
    }, [id])

    useEffect(() => {

        if (pattern.test(email) && authorName !== "") {
            setIsSave(false)
            document.getElementById('validate-email').innerText = ""
        }
        else{
            setIsSave(true)
            
        }
        
    })

    const onUpdate = (item) => {
        document.getElementById('name').value = item.name
        document.getElementById('email').value = item.email
        document.getElementById('hidden-id').value = item._id
        // document.getElementById('image').value = item.image

        setId(document.getElementById('hidden-id').value)
        setauthorName(document.getElementById('name').value)
        setemail(document.getElementById('email').value)

        let tempImage = item.image === "" ? placeholder : item.image
        console.log(tempImage);
        setBrowsedImage(item.image)
        // setMyImage(tempImage)
        console.log("BrowsImage ", browsedImage)
        console.log("MyImage ", MyImage)
    }

    const onClear = () => {
        document.getElementById('name').value = ""
        document.getElementById('email').value = ""
        document.getElementById('hidden-id').value = ""
        document.getElementById('image-preview').src = placeholder
        setId(" ")
        setId("")
        setauthorName("")
        setemail("")
        setMyImage("")
    }

    const onDelete = async (idx) => {
        const temp = await deleteAuthorById(idx)
        alert(temp.data.message)
        let newAuthor = author.filter(item => {
            return item._id !== idx
        })
        setauthor(newAuthor)
        onClear()
    }

    const onNameChange = (name) => {
        setauthorName(name)
        if (name === "")
            document.getElementById('validate-name').innerText = "Name Cannot be Empty!!"
        else document.getElementById('validate-name').innerText = ""
    }

    const onEmailChange = (email) => {
        setemail(email)
        if(pattern.test(email))
            document.getElementById('validate-email').innerText = ""
        else
            document.getElementById('validate-email').innerText = "Invalid Email!!"

    }

    const onAddOrEdit = async () => {
        try {

            if (id === "") {
                const url = MyImage && await uploadImage(MyImage)
                let author = {
                    name: authorName,
                    email,
                    image: url
                }
                console.log("Add");
                let temp = await fetchAddAuthor(author)
                alert(temp.data.message)

            }
            else {
                const url = MyImage && await uploadImage(MyImage)
                let author = {
                    name: authorName,
                    email,
                    image: url ? url : browsedImage
                }
                console.log("Edit", author.image);
                let temp = await upadateAuthorById(id, author)
                alert(temp.data.message)
            }
            document.getElementById('hidden-id').value = ""
        } catch (error) {
            console.log("onAddOrEdit: ", error);
        }
        onClear()
    }

    const onBrowsedImage = (e) => {
        setMyImage(e.target.files[0]);
        setBrowsedImage(URL.createObjectURL(e.target.files[0]))
    }


    return (
        <div>
            <h1>Author</h1>
            <Row>
                <Form className="col-9">
                    <input type="text" className="d-none" id="hidden-id"></input>
                    <Form.Group className="mb-3">
                        <Form.Label>Author Name</Form.Label>
                        <Form.Control type="text" placeholder="Author Name"
                            id="name"
                            onChange={(e) => onNameChange(e.target.value)}
                        />
                        <Form.Text className="text-danger" id="validate-name"></Form.Text>
                    </Form.Group>

                    <Form.Group className="mb-3" >
                        <Form.Label>Email</Form.Label>
                        <Form.Control type="email" placeholder="Email"
                            id="email"
                            onChange={(e) => onEmailChange(e.target.value)}
                        />
                        <Form.Text className="text-danger" id="validate-email"></Form.Text>
                    </Form.Group>
                    <Button variant="primary" onClick={onAddOrEdit} disabled={isSave}>
                        {id === "" ? "Add" : "Save"}
                    </Button>{` `}
                    <Button variant="warning" onClick={onClear}>Clear</Button>
                </Form>
                <div className="col-3">
                    <label htmlFor="image">
                        <img className="w-100" src={browsedImage === "" ? placeholder : browsedImage} id="image-preview" />
                    </label>
                    <input onChange={(e) => onBrowsedImage(e)} type="file" id="image" style={{ display: 'none'}}/>
                    <p>{browsedImage === "" ? "Please Choose Image" : ""}</p>
                    <Button onClick={() => { 
                        document.getElementById('image-preview').src = placeholder
                        setBrowsedImage("")
                        }} variant="warning">Clear Photo</Button>

                </div>
            </Row>
            <br />

            {/* Table */}
            <Table striped bordered hover>
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Author Name</th>
                        <th>Email</th>
                        <th>Photo</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        author.map((item, index) => (
                            <tr key={index}>
                                <td>{index + 1}</td>
                                <td>{item.name}</td>
                                <td>{item.email}</td>
                                <td>
                                    <img src={item.image} alt="" style={{ height: '70px' }} />
                                </td>
                                <td>
                                    <Button disabled={id === "" ? false : true} onClick={() => onUpdate(item)} variant="warning" >Edit</Button>{` `}
                                    <Button disabled={id === "" ? false : true} onClick={() => onDelete(item._id)} variant="danger">Delete</Button>
                                </td>
                            </tr>
                        ))
                    }

                </tbody>
            </Table>
        </div>
    )
}
