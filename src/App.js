import React from 'react'
import { Container } from 'react-bootstrap'
import MyAuthor from './components/MyAuthor'
import MyNavbar from './components/MyNavbar'

export default function App() {
    return (
        <div>
            <MyNavbar />
            <Container>
                <MyAuthor />
                {/* <MyTable /> */}
            </Container>
        </div>
    )
}
